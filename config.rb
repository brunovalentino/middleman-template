### 
# Compass
###

# Change Compass configuration
compass_config do |config|
  config.output_style = :expanded
  config.sass_options = {:debug_info => true}
end

# Compass Options
set :css_dir, 'assets/css'
set :js_dir, 'assets/js'
set :images_dir, 'assets/img'

# Clean URLs
#activate :directory_indexes

# LiveReload
activate :livereload

# i18n
activate :i18n

sprockets.append_path File.join "#{root}", "bower_components"

# Build-specific configuration
configure :build do
  compass_config do |config|
    config.sass_options = {:debug_info => false}
    config.sass_options = {:line_comments => false}
    config.output_style = :compressed
  end

  activate :minify_css
end