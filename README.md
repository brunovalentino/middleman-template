Middleman Template
===========

Just a simple starting point for middleman. 
Includes LiveReload, Compass, I18N, and Local Data.

## Requirements

* Middleman
* Bundler
  - `gem install bundler`
* Bower

## Setup

1. Clone this repository:
	- `git clone git@github.com:bvalentino/middleman-template.git new-project`
2. Install necessary gems:
	- `bundle install`

## Run

Execute `bundle exec middleman`. This will start a local web server running at: [http://localhost:4567/](http://localhost:4567/).

## Sitemap

[http://localhost:4567/__middleman/sitemap/](http://localhost:4567/__middleman/sitemap/).
Always good to keep it a click away when developing.
